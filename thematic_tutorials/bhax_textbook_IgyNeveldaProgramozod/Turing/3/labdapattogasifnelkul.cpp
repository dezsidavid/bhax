#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int main(void)
{
    WINDOW* ablak = initscr();
    noecho();
    cbreak();
    nodelay(ablak, true);

    int x = 0;    int x2 = 0;
    int y = 0;    int y2 = 0;
    int maxx = 79 * 2;
    int maxy = 20 * 2;

    for (;; ) 
    {
        x = (x - 1) % maxx;
        x2 = (x2 + 1) % maxx;

        y = (y - 1) % maxy;
        y2 = (y2 + 1) % maxy;

        clear();

        mvprintw(0, 0, "-------------------------------------------------------------------------------");
        mvprintw(20, 0, "-------------------------------------------------------------------------------");
        mvprintw(abs((y + (maxy - y2)) / 2), abs((x + (maxx - x2)) / 2), "labda");

        refresh();
        usleep(50000);
    }
    return 0;
}