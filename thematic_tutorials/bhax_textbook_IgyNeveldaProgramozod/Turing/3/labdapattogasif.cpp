#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main(void)
{
    WINDOW* ablak = initscr();
    int x = 0;  int x2 = 1;   int maxx;
    int y = 0;  int y2 = 1;   int maxy;

    for (;;) 
    {
        getmaxyx(ablak, maxx, maxy);
        mvprintw(y, x, "(^o^)");

        refresh();
        usleep(100000);

        clear();

        x = x + x2;
        y = y + y2;

        if (x >= maxx - 1)
        {
            x2 = x2 * -1;
        }
        if (x <= 0) 
        {
            x2 = x2 * -1;
        }
        if (y <= 0) 
        {
            y2 = y2 * -1;
        }
        if (y >= maxy - 1)
        {
            y2 = y2 * -1;
        }
    }
    return 0;
}