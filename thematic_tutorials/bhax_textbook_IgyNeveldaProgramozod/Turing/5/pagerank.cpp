#include <stdio.h>
#include <math.h>

void kiir(double v[], int k)
{
    int i;
    for (i = 0; i < k; i++) 
    {
        printf("PageRank [%d]: %lf\n", i, v[i]);
    }
}

double tavolsag(double pagerank[], double pagerank2[], int n)
{
    double osszeg = 0;
    for (int i = 0; i < n; ++i) 
    {
        osszeg += (pagerank2[i] - pagerank[i]) * (pagerank2[i] - pagerank[i]);
    }
    return sqrt(osszeg);
}

int main(void)
{
    double adatok[4][4] =
    {
            {0.0,   0.0,         1.0 / 3.0,   0.0},
            {1.0,   1.0 / 2.0,   1.0 / 3.0,   1.0},
            {0.0,   1.0 / 2.0,   0.0,         0.0},
            {0.0,   0.0,         1.0 / 3.0,    0.0}
    };
    double pagerank[4] = 
    {
        0.0, 0.0, 0.0, 0.0
    };
    double pagerank2[4] = 
    { 
        1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0, 1.0 / 4.0 
    };

    int i, j;
    for (;;) {
        for (i = 0; i < 4; ++i)
        {
            pagerank[i] = 0.0;
            for (j = 0; j < 4; ++j)
            {
                pagerank[i] += (adatok[i][j] * pagerank2[j]);
            }
        }
        if (tavolsag(pagerank, pagerank2, 4) < 0.0000000001)
        {
            break;
        }
        for (i = 0; i < 4; ++i)
        {
            pagerank2[i] = pagerank[i];
        }
    }
    kiir(pagerank, 4);
    return 0;
}