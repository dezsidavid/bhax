#include <stdio.h>

//7.
int* f();

int main(void)
{
	//1. 
	int x;
	//2.
	int* y;
	//3.
	int* z = &x;
	//4.
	int a[10];
	//5.
	int* uh = &a[5];
	//6.
	int* pp[5];
	//8.
	int* (*f_ptr)();
	//9.
	int (*f2(int z3))(int z1, int z2);
	//10.
	int (*(*f3) (int u3))(int u1, int u2);

	return 0;
}